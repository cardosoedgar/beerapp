//
//  PunkAPITests.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 21/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Nimble
import Quick

@testable import BeerApp

class PunkAPITest: QuickSpec {
    override func spec() {
        describe("PunkAPI") {
            
            var punkAPI: PunkAPIRequest!
            
            beforeEach {
                punkAPI = PunkAPIRequest()
            }
            
            it("should return nil for invalid json") {
                let json = Json(json: JsonHelper.readJson(name: "invalid"))
                punkAPI.networkRequest = NetworkRequestMock(json: json)
                var response: [JsonObject]? = [JsonObject]()
                
                punkAPI.getBeers(onPage: 0) { jsonObject in
                    response = jsonObject
                }
                
                expect(response).toEventually(beNil())
            }
            
            it("should return nil for error conection") {
                punkAPI.networkRequest = NetworkRequestMock(error: true)
                var response: [JsonObject]? = [JsonObject]()
                
                punkAPI.getBeers(onPage: 0) { jsonObject in
                    response = jsonObject
                }
                
                expect(response).toEventually(beNil())
            }
            
            it("should return jsonobject for valid json") {
                let json = Json(json: JsonHelper.readJson(name: "beers"))
                punkAPI.networkRequest = NetworkRequestMock(json: json)
                var response: [JsonObject]? = nil
                
                punkAPI.getBeers(onPage: 1) { jsonObject in
                    response = jsonObject
                }
                
                expect(response).toEventuallyNot(beNil())
            }
        }
    }
}
