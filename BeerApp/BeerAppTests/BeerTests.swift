//
//  BeerTests.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 21/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Nimble
import Quick

@testable import BeerApp

extension Beer {
    static func validJson() -> JsonObject {
        return [
            "id": 1,
            "name": "Beer",
            "tagline": "nice beer to drink",
            "abv": Float(5.4),
            "ibu": Float(40),
            "image_url": "image.jpg",
            "description": "this is a description"
        ]
    }
}

class BeerTests: QuickSpec {
    override func spec() {
        describe("Beer") {
            
            var json: JsonObject!
            
            beforeEach {
                json = Beer.validJson()
            }
            
            it("should parse beer json") {
                let beer = Beer(json: json)
                
                expect(beer?.name) == "Beer"
                expect(beer?.image_url) == "image.jpg"
                expect(beer?.tagline) == "nice beer to drink"
                expect(beer?.abv) == 5.4
                expect(beer?.ibu) == 40
                expect(beer?.description) == "this is a description"
            }
            
            it("should not parse for nil property") {
                json.removeValue(forKey: "name")
                
                let beer = Beer(json: json)
                
                expect(beer).to(beNil())
            }
            
            it("should parse an array of beer") {
                let array: [JsonObject] = [json, json, json]
                
                let beers = Beer.load(jsonArray: array)
                
                expect(beers.count) == 3
            }
        }
    }
}
