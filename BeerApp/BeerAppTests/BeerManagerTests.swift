//
//  BeerManagerTests.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 21/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Nimble
import Quick

@testable import BeerApp

class BeerManagerTest: QuickSpec {
    override func spec() {
        describe("BeerManager") {
            
            var beerManager: BeerManager!
            var punkAPI = PunkAPIRequest()
            
            beforeEach {
                beerManager = BeerManager()
                punkAPI.networkRequest = NetworkRequestMock()
                beerManager.punkAPI = punkAPI
            }
            
            it("should load beers") {
                let json = Json(json: JsonHelper.readJson(name: "beers"))
                punkAPI.networkRequest = NetworkRequestMock(json: json)
                var success = false
                
                beerManager.loadBeers(page: 1) { result in
                    success = result
                }
                
                expect(success).toEventually(beTrue())
            }
            
            it("should return false when an error occur on loadBeers method") {
                punkAPI.networkRequest = NetworkRequestMock(error: true)
                var success = true
                
                beerManager.loadBeers(page: 1) { result in
                    success = result
                }
                
                expect(success).toEventually(beFalse())
            }
            
            it("should give Beer at Index") {
                let json = Json(json: JsonHelper.readJson(name: "beers"))
                punkAPI.networkRequest = NetworkRequestMock(json: json)
                var beer: Beer? = nil
                
                beerManager.loadBeers(page: 1) { success in
                    beer = beerManager.beerAt(index: 0)
                }
                
                expect(beer).toEventuallyNot(beNil())
            }
            
            it("should give nil to unkown index") {
                let json = Json(json: JsonHelper.readJson(name: "valid"))
                punkAPI.networkRequest = NetworkRequestMock(json: json)
                var beer: Beer? = Beer(json: [String:Any]())
                
                beerManager.loadBeers(page: 1) { success in
                    beer = beerManager.beerAt(index: 100)
                }
                
                expect(beer).toEventually(beNil())
            }
        }
    }
}
