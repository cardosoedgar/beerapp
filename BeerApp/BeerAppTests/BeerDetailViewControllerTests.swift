//
//  BeerDetailViewController.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 21/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Nimble
import Quick

@testable import BeerApp

class BeerDetailViewControllerTest: QuickSpec {
    override func spec() {
        describe("BeerDetailViewController") {
            
            var beerVC: BeerDetailViewController!
            
            beforeEach {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                beerVC = storyboard.instantiateViewController(withIdentifier: "BeerDetailViewController") as! BeerDetailViewController
                
                beerVC.loadView()
            }
            
            it("should populate labels") {
                let beer = Beer(json:Beer.validJson())
                
                beerVC.beer = beer
                beerVC.setupBeer()
                
                expect(beerVC.title) == "Beer"
                expect(beerVC.taglineLabel.text).toNot(beNil())
            }
            
            it("should not populate labels") {
                beerVC.setupBeer()
                
                expect(beerVC.taglineLabel.text) == ""
            }
            
        }
    }
}
