//
//  BeerCellTests.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 21/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Nimble
import Quick

@testable import BeerApp

class BeerCellTest: QuickSpec {
    override func spec() {
        describe("BeerCell") {
            
            var beerCell: BeerCell!
            
            beforeEach {
                beerCell = Bundle.main.loadNibNamed("BeerCell", owner: nil, options: nil)?.first as! BeerCell
            }
            
            it("should populate labels") {
                let beer = Beer(json:Beer.validJson())
                
                beerCell.setup(beer: beer)
                
                expect(beerCell.nameLabel.text).toNot(beNil())
            }
            
            it("should not populate labels") {
                beerCell.setup(beer: nil)
                
                expect(beerCell.nameLabel.text) == ""
            }
        }
    }
}
