//
//  AppDelegate.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 19/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

