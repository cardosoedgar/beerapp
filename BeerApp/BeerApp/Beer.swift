//
//  Beer.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 19/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Foundation

class Beer {
    
    let id: Int
    let name: String
    let description: String
    let image_url: String
    let tagline: String
    let abv: Float
    let ibu: Float
    
    init?(json: JsonObject) {
        guard let id = json["id"] as? Int,
            let name = json["name"] as? String,
            let tagline = json["tagline"] as? String,
            let description = json["description"] as? String,
            let image_url = json["image_url"] as? String,
            let abv = json["abv"] as? Float,
            let ibu = json["ibu"] as? Float
        else {
            return nil
        }
        
        self.id = id
        self.name = name
        self.description = description
        self.image_url = image_url
        self.abv = abv
        self.ibu = ibu
        self.tagline = tagline
    }
    
    class func load(jsonArray: [JsonObject]) -> [Beer] {
        var beers = [Beer]()
        
        for obj in jsonArray {
            if let beer = Beer(json: obj) {
                beers.append(beer)
            }
        }
        
        return beers
    }
}
