//
//  PunkAPIRequest.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 19/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Foundation

class PunkAPIRequest {
    
    var networkRequest: NetworkRequestProtocol = NetworkRequest()
    let baseURL = "https://api.punkapi.com/v2/beers"
    
    func getBeers(onPage page: Int, completion: @escaping ([JsonObject]?) -> Void) {
        
        let url = URL(string: baseURL + "?page=\(page)")!
        
        networkRequest.request(url, method: .get, parameters: nil, headers: nil) { result in
            switch result {
            case .success(.array (let response)):
                completion(response)
            default:
                completion(nil)
            }
        }
    }
}
