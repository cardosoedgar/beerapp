//
//  BeerCell.swift
//  TwitchTvApp
//
//  Created by Edgar Cardoso on 04/06/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import UIKit
import Kingfisher

class BeerCell: UICollectionViewCell, ReusableCell, LoadNib {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var abvLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    func setup(beer: Beer?) {
        guard let beer = beer else {
            return
        }
        
        let imageUrl = URL(string: beer.image_url)
        image.kf.setImage(with: imageUrl)
        nameLabel.text = beer.name
        abvLabel.text = "abv: \(beer.abv)"
    }
}
