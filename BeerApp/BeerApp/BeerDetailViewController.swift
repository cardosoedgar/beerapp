//
//  BeerDetailViewController.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 20/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import UIKit

class BeerDetailViewController: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var taglineLabel: UILabel!
    @IBOutlet weak var abvLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    var beer: Beer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBeer()
    }
    
    func setupBeer() {
        guard let beerImage = beer?.image_url,
            let imageURL = URL(string: beerImage) else {
            return
        }
        
        self.title = beer?.name
        self.image.kf.setImage(with: imageURL)
        self.taglineLabel.text = beer?.tagline
        self.abvLabel.text = "ABV: \(beer?.abv ?? 0) / IBU: \(beer?.ibu ?? 0)"
        self.descriptionLabel.text = beer?.description
    }
}
