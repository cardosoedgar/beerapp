//
//  BeerManager.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 20/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import Foundation

class BeerManager {
    
    var punkAPI = PunkAPIRequest()
    
    private var beers = [Beer]()
    
    func loadBeers(page: Int, completion: @escaping (Bool) -> Void) {
        punkAPI.getBeers(onPage: page) { jsonObject in
            guard let json = jsonObject else {
                completion(false)
                return
            }
            
            if (page == 1) {
                self.beers = Beer.load(jsonArray: json)
            } else {
                self.beers.append(contentsOf: Beer.load(jsonArray: json))
            }
            completion(true)
        }
    }
    
    func beerAt(index: Int) -> Beer? {
        if index >= 0 && index < beers.count {
            return beers[index]
        }
        
        return nil
    }
    
    func beersCount() -> Int {
        return beers.count
    }
}
