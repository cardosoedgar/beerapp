//
//  ViewController.swift
//  BeerApp
//
//  Created by Edgar Cardoso on 19/07/17.
//  Copyright © 2017 Edgar Cardoso. All rights reserved.
//

import UIKit

class BeerListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    let refreshControl = UIRefreshControl()
    
    var didFinishLoading = true
    var page = 1
    let beerManager = BeerManager()
    let punkAPI = PunkAPIRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupPullToRefresh()
        loadBeers()
    }
    
    func loadBeers() {
        beerManager.loadBeers(page: page) { success in
            if success {
                self.collectionView.reloadData()
            } else {
                self.showAlert(withTitle: "Erro", andMessage: "Ocorreu um erro ao tentar carregar as cervejas. Tente novamente mais tarde.")
            }
            
            self.didFinishLoading = true
            self.refreshControl.endRefreshing()
        }
    }
    
    func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(cellType: BeerCell.self)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "BeerDetailViewController") as? BeerDetailViewController else {
            return
        }
        
        vc.beer = beerManager.beerAt(index: indexPath.row)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return beerManager.beersCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: BeerCell.self)
        let beer = beerManager.beerAt(index: indexPath.row)
        cell.setup(beer: beer)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width
        return CGSize(width: width/2, height: width/2)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            if self.didFinishLoading {
                self.didFinishLoading = false
                self.page += 1
                self.loadBeers()
                self.collectionView.reloadData()
            }
        }
    }
    
    func setupPullToRefresh() {
        if #available(iOS 10.0, *) {
            self.collectionView.refreshControl = refreshControl
        } else {
            self.collectionView.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    func pullToRefresh() {
        self.page = 1
        self.loadBeers()
    }
}

